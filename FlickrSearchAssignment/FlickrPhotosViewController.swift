//
//  FlickrPhotosViewController.swift
//  FlickrSearchAssignment
//
//  Created by Matt DeBoer on 2/25/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

final class FlickrPhotosViewController: UICollectionViewController {
    
    // MARK: - Properties
    fileprivate let reuseIdentifier = "FlickrCell"
    fileprivate var searches = [FlickrSearchResults]()
    fileprivate let flickr = Flickr()
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    fileprivate let itemsPerRow: CGFloat = 3
}
    extension FlickrPhotosViewController : UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            // 1
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            textField.addSubview(activityIndicator)
            activityIndicator.frame = textField.bounds
            activityIndicator.startAnimating()
            
            flickr.searchFlickrForTerm(textField.text!) {
                results, error in
                
                activityIndicator.removeFromSuperview()
                
                if let error = error {
                    // 2  Log any errors to the console. Obviously, in a production application you would want to display these errors to the user.
                    print("Error searching : \(error)")
                    return
                }
                
                if let results = results {
                    // 3  The results get logged and added to the front of the searches array
                    print("Found \(results.searchResults.count) matching \(results.searchTerm)")
                    self.searches.insert(results, at: 0)
                    
                    // 4  new data and need to refresh the UI. Y
                    self.collectionView?.reloadData()
                }
            }
            
            textField.text = nil
            textField.resignFirstResponder()
            return true
        }
    }

// MARK: - UICollectionViewDataSource
extension FlickrPhotosViewController {
    //1  one search per section, so the number of sections is the count of the searches array.
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return searches.count
    }
    
    //2  number of items in a section is the count of the searchResults array from
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return searches[section].searchResults.count
    }

    //3  placeholder method just to return a blank cell
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //1
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! FlickrPhotoCell
        //2
        let flickrPhoto = photoForIndexPath(indexPath: indexPath)
        cell.backgroundColor = UIColor.white
        //3
        cell.imageView.image = flickrPhoto.thumbnail
        
        return cell
    }
}
//    func prepare(for segue: UICollectionView, sender: Any?){
    
//}

        func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let secondVC = segue.destination as! ViewController
            secondVC.searches = sender as! [FlickrSearchResults]
}

// MARK: - Private
private extension FlickrPhotosViewController {
    func photoForIndexPath(indexPath: IndexPath) -> FlickrPhoto {
        return searches[(indexPath as NSIndexPath).section].searchResults[(indexPath as IndexPath).row]
    }
}


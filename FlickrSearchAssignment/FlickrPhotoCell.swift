//
//  FlickrPhotoCell.swift
//  FlickrSearchAssignment
//
//  Created by Matt DeBoer on 2/25/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
